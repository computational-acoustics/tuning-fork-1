# Tuning Fork 1

ElmerFEM model for the elastic modes of a tuning fork.

Part of the [accompanying repositories](https://gitlab.com/computational-acoustics) of the [Computational Acoustics with Open Source Software](https://computational-acoustics.gitlab.io/website/) project.

## Covering Episodes

* [Tuning Fork - Part 1](https://computational-acoustics.gitlab.io/website/posts/25-tuning-fork-part-1/).
* [Tuning Fork - Part 2](https://computational-acoustics.gitlab.io/website/posts/26-tuning-fork-part-2/).

## Study Summary

The main parameters of the study are reported below.

### Domain

$`512`$ $`text{Hz}`$ Tuning Fork by [Justin Black](http://justinablack.com/tuning_fork_freqs/).

| Shape       | Size                                      | Mesh Algorithm  | Mesh Min. Size        | Mesh Max. Size        | Element Order           |
|-------------|-------------------------------------------|-----------------|-----------------------|-----------------------|-------------------------|
| Tuning Fork | $`164`$ X $`25`$ X $`9.56`$ $`\text{mm}`$ | NETGEN 1D-2D-3D | $`1`$ $`\text{mm}`$   | $`1.5`$ $`\text{mm}`$ | Second ($`p`$-elements) |

### Material

Aluminum 6061

| Property        | Value                                     |
|-----------------|-------------------------------------------|
| Density         | $`2700`$ $`\frac{\text{kg}}{\text{m}^3}`$ |
| Young's Modulus | $`70 \cdot 10^{9}`$ $`\text{Pa}`$         |
| Poisson's Ratio | $`0.35`$                                  |

### Boundary Conditions

Different studies with different boundary conditions:

| Study                            | Boundary Condition           dual |
|----------------------------------|-------------------------------|
| `elmerfem/case_free.sif`         | Free                          |
| `elmerfem/case_bottom_prong.sif` | Bottom prong simply supported |
| `elmerfem/case_whole_prong.sif`  | Whole prong simply supported  |

## Software Overview

The table below reports the software used for this project.

| Software                                           | Usage                        | Version |
|----------------------------------------------------|------------------------------|---------|
| [FreeCAD](https://www.freecadweb.org/)             | 3D Modeller                  | 0.19    |
| [Salome Platform](https://www.salome-platform.org) | Pre-processing               | 9.6.0   |
| [ElmerFEM](http://www.elmerfem.org)                | Multiphysical solver         | 9.0     |
| [ParaView](https://www.paraview.org/)              | Post-processing              | 5.9.0   |
| [Julia](https://julialang.org/)                    | Technical Computing Language | 1.5.3   |

## Repo Structure

* `elmerfem` contains the Elmer project.
* `geometry.FCStd` is the FreeCAD geometry model. This file can be used to export geometric entities to _BREP_ files to pre-process with Salome. _BREP_ files are excluded from the repo as they are redundant.
* `study.hdf` is the Salome study of the geometry. It contains the geometry pre-processing and meshing. Note that the mesh in this file is **not** computed.
* The `*.pvsm` files are ParaView state files. They can be loaded into ParaView to plot the solution.
* `camera.pvcc` contains camera settings for ParaView.
* The `make_animations.jl` file contains Julia code to postprocess the Elmer results. It will produce a timestampend `vtu` series which animates the system motion. The results are stored in the `animations` folder.
* The `run.sh` script allows to run the study and produce the animations, provided that your Julia environment has the required packages (see below).

The repo contains only the _source_ of the simulation. To obtain results, the study must be solved.

## How to Run this Study

Follow the steps below to run the study.

Clone the repository and `cd` into the cloned directory:

```bash
git clone https://gitlab.com/computational-acoustics/tuning-fork-1.git
cd tuning-fork-1/
```

`cd` in the `elmerfem` directory and run the various studies:

```bash
cd elmerfem/
ElmerSolver case_free.sif
ElmerSolver case_bottom_prong.sif
ElmerSolver case_whole_prong.sif
```

When finished, open ParaView and select `File > Load State` to load the `.pvsm` files in the root of the repository. To load the ParaView state successfully, choose _Search files under specified directory_ as shown in the example below. The folder specified in `Data Directory` should be the root folder of the repo (blurred below as the absolute path will differ in your machine).

![Load State Example](res/pictures/paraview-load-state.png)

To run `make_animations.jl` your julia environment needs the [FEATools](https://gitlab.com/computational-acoustics/featools.jl) package. After this package is installed in your Julia environment return to the repository root and issue this command:

```bash
cd ..
julia make_animations.jl
```

The staps above are automatised by the `run.sh` bash script.

