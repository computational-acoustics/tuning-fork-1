using FEATools


function make_animation(
    source_file::String,
    destination_file::String,
    number_of_periods::Integer,
    frames_per_period::Integer
    )
    
    source_data = load_mesh(source_file)
    destination_data = load_mesh(source_file)
    
    n_frames = frames_per_period * number_of_periods
    l_pad_size = ceil(Int, log10(n_frames))
    
    for f in 0:(n_frames - 1)
    
        destination_dict = source_data.point_data
        
        for (k, _) in destination_dict
           destination_dict[k] .*= cospi(2 * f / frames_per_period)
        end
        
        destination_data.point_data = destination_dict
        
        write_mesh(
            destination_file * "_t" * lpad(f, l_pad_size, "0") * ".vtu",
            destination_data
        )

    end
    
end

make_animation(
    "elmerfem/case_free_t0001.vtu", 
    "animations/case_free", 
    1, 
    24 * 2
)

make_animation(
    "elmerfem/case_bottom_prong_t0001.vtu", 
    "animations/case_bottom_prong", 
    1, 
    24 * 2
)

make_animation(
    "elmerfem/case_whole_prong_t0001.vtu", 
    "animations/case_whole_prong", 
    1, 
    24 * 2
)

