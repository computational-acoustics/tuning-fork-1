Sometimes `salomeToElmer` seems to make meshes to produce strange results (flipping of the order of the modes in the output `vtu`. Export to `unv` and use `ElmerGrid 8 2 file.unv` to convert to Elmer format.

Use this to repeat videos containing just one period:

```bash
ffmpeg -stream_loop 10 -i free.avi -c copy free_loop.avi
```

