#!/bin/bash

cd elmerfem
ElmerSolver case_free.sif
ElmerSolver case_bottom_prong.sif
ElmerSolver case_whole_prong.sif

cd ..
julia make_animations.jl

